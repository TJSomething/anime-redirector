{-# LANGUAGE DeriveGeneric #-}

module Lib
  ( module Lib,
  )
where

import Data.Aeson.Types
import qualified Data.Text as T
import GHC.Generics

newtype AnimeAttributes = AnimeAttributes
  { slug :: T.Text
  }
  deriving (Generic, Show)

instance FromJSON AnimeAttributes

newtype AnimeData = AnimeData
  { attributes :: AnimeAttributes
  }
  deriving (Generic, Show)

instance FromJSON AnimeData

newtype AnimeMeta = AnimeMeta {count :: Int} deriving (Generic, Show)

instance FromJSON AnimeMeta

data AnimeResponse = AnimeResponse
  { _data :: [AnimeData],
    _meta :: AnimeMeta
  }
  deriving (Generic, Show)

instance FromJSON AnimeResponse where
  parseJSON =
    genericParseJSON
      defaultOptions
        { fieldLabelModifier = drop 1
        }
