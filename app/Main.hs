{-# LANGUAGE OverloadedStrings #-}
{-# LANGUAGE QuasiQuotes #-}
{-# LANGUAGE ScopedTypeVariables #-}
{-# LANGUAGE TemplateHaskell #-}

module Main where

import Control.Monad.IO.Class
import Data.Aeson
import Data.Aeson.Types
import qualified Data.ByteString as BS
import qualified Data.ByteString.Builder as BSL
import qualified Data.ByteString.Lazy as BSL
import Data.FileEmbed
import Data.IORef
import qualified Data.List as L
import Data.Maybe
import Data.String.Conversions
import qualified Data.Text as T
import qualified Data.Text.Encoding as BS
import qualified Data.Text.Encoding as BSL
import qualified Data.Text.Internal.Builder as BSL
import qualified Data.Text.Lazy as TL
import Data.Text.Lazy.Encoding
import GHC.Generics
import qualified Lib
import qualified NeatInterpolation as NI
import Network.HTTP.Client.Conduit (Response (responseBody))
import Network.HTTP.Simple
import System.Environment
import System.Random
import System.Random.Stateful
import Text.Read (readMaybe)
import Web.Scotty

createRequest services offset = do
  let streamingServicesParam =
        ( [ ("filter[streamers]", Just $ BS.intercalate "," services)
            | not (null services)
          ]
        )
  request <- parseRequest "https://kitsu.io/api/edge/anime"
  return $
    setRequestHeader "Accept" [BS.encodeUtf8 "application/vnd.api+json"] $
      setRequestQueryString
        ( [ ("fields[anime]", Just "slug"),
            ("page[limit]", Just "1"),
            ("page[offset]", Just $ BS.encodeUtf8 offset),
            ("sort", Just "-user_count")
          ]
            ++ streamingServicesParam
        )
        request

streams =
  [ "Hulu",
    "Funimation",
    "Crunchyroll",
    "CONtv",
    "Netflix",
    "HIDIVE",
    "TubiTV",
    "Amazon",
    "YouTube",
    "AnimeLab",
    "VRV"
  ]

css :: TL.Text
css =
  face <> originalCss
  where
    fonts =
      [ "-apple-system",
        "BlinkMacSystemFont",
        "avenir next",
        "avenir",
        "segoe ui",
        "helvetica neue",
        "helvetica",
        "Cantarell",
        "Ubuntu",
        "roboto",
        "noto",
        "arial",
        "sans-serif"
      ]
    fontSrc = TL.intercalate "," $ ["local(\"" <> font <> "\")" | font <- fonts]
    -- Lit has hard-coded the Nunito font family. We're replacing that with a native
    -- stack.
    face = "@font-face{font-family:nunito;src:" <> fontSrc <> ";}"
    originalCss = TL.fromStrict $ BS.decodeUtf8 $(makeRelativeToProject "css/dist/lit.css" >>= embedFile)

wrap body =
  [NI.text|
    <!DOCTYPE html>
    <html lang="en">
    <head>
      <meta charset="UTF-8">
      <meta name="viewport" content="width=device-width, initial-scale=1.0">
      <meta http-equiv="X-UA-Compatible" content="ie=edge">
      <title>Random Anime</title>
      <link rel="stylesheet" href="/style.css">
    </head>
      $body
    </html>
  |]

main :: IO ()
main = do
  gen <- getStdGen
  genRef <- newIOGenM gen
  envPort <- lookupEnv "PORT"
  let port = fromMaybe 8080 $ envPort >>= readMaybe
  scotty port $ do
    get "/" $ do
      let checkboxes =
            T.concat $
              [ [NI.text|
                  <li>
                    <label>
                      <input
                        type="checkbox"
                        name="streamers"
                        value="$stream"
                      > $stream
                    </label>
                  </li>
                |]
                | stream <- streams
              ]

      html $
        cs $
          wrap
            [NI.text|
              <body class="c">
                <h1>Random Anime</h1>
                <h2>Pick your streaming sites</h2>
                <form action="/random">
                  <ul>
                    $checkboxes
                  </ul>
                  <input type="submit" class="btn primary" value="SPIN">
                </form>
              </body>
            |]

    get "/style.css" $ do
      setHeader "Content-Type" "text/css"
      text css

    get "/random" $ do
      paramsList <- params

      let streamersList = [snd param | param <- paramsList, fst param == "streamers"]

      let streamersParam =
            [cs $ TL.intercalate "," streamersList | not $ null streamersList]

      enumerateReq <- createRequest streamersParam "0"
      enumerateResp :: Response Lib.AnimeResponse <- httpJSON enumerateReq

      let animeCount = Lib.count $ Lib._meta $ responseBody enumerateResp
      offset <- uniformRM (0, animeCount - 1) genRef

      randomReq <- createRequest streamersParam $ cs $ show offset
      randomResp :: Response Lib.AnimeResponse <- httpJSON randomReq

      let randomSlug =
            Lib.slug $
              Lib.attributes $
                head $
                  Lib._data $
                    responseBody
                      randomResp

      setHeader "Cache-Control" "no-cache"

      html $
        cs $
          wrap
            [NI.text|
              <body>
                <style>
                  html, body, iframe {
                    height: 100%;
                    margin: 0;
                  }
                  body {
                    display: flex;
                    flex-direction: column;
                  }
                </style>
                <script>
                  function spin() {
                    if (location.search) {
                      location.search = location.search.replace(/$|&cb=.*/, '&cb=' + Date.now().toString());
                    } else {
                      location.search = 'cb=' + Date.now().toString();
                    }
                  }
                </script>
                <div class="c">
                  <button class="btn primary" onclick="spin()">SPIN AGAIN</button>
                  <a class="btn" href="/">Change streams</a>
                </div>
                <iframe
                  width="100%"
                  scrolling="yes"
                  frameborder="0" 
                  name="$randomSlug"
                  src="https://kitsu.io/anime/$randomSlug"></iframe>
              </body>
            |]
