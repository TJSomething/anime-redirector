#!/usr/bin/env bash

set -euo pipefail

stack build
mkdir -p dist
cp "$(stack exec which anime-redirector-exe)" dist/
fly deploy
